# HWJS1


1. Як можна оголосити змінну у Javascript?
Змінні в Javascript можна оголосити за допомогою var, let та const. 
const – це як let, але значення змінної не може змінюватися. Також змінну можна оголосити за допомогою ключового слова var майже таке, як let. Воно теж оголошує змінну, але дещо іншим, “застарілим” способом.
2. У чому різниця між функцією prompt та функцією confirm?
prompt показує модульне вікно з повідомленням і полем куди відвідувач може ввести текст та кнопками ОК/Скасувати. confirm виводить лише повідомлення, яке можна підвердити або скасувати.

3. Що таке неявне перетворення типів? Наведіть один приклад.
Неявне перетворення - це те перетворення яке відбулося як результат дій, але непрямий результат, а як наслідок інших дій. Наприклад: 
alert ('1' + 2) // 12
